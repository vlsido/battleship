﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
 using System.Diagnostics;
 using CalculatorBrain;
 using MenuSystem;

 namespace ConsoleApp
{
    class Program
    {
      private static readonly Brain Brain = new Brain(); 
        static void Main(string[] args)
        {
            Console.Clear();

            var mainMenu = new Menu(ReturnCurrentDisplayValue,"Calculator Main", EMenuLevel.Root);
            mainMenu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("A", "Binary operations", SubmenuBinary),
                new MenuItem("S", "Unary operations", SubmenuUnary),
            });
            
            mainMenu.Run();
            
            Console.ResetColor();
        }

        public static string ReturnCurrentDisplayValue()
        {
            return Brain.CurrentValue.ToString();
        }

        public static string SubmenuBinary()
        {
            var menu = new Menu(ReturnCurrentDisplayValue,"Binary", EMenuLevel.First);
            menu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("+", "+", Add),
                new MenuItem("-", "-", Substract),
                new MenuItem("/", "/", Divide),
                new MenuItem("*", "*", Multiply),
                new MenuItem("p", "x^y", Power),
            });
            var res = menu.Run();
            return res;
        }

        public static string Add()
        {
            Console.WriteLine("plus");
            Console.Write("number: ");
            var n = Console.ReadLine()?.Trim();
            double.TryParse(n, out var converted);

            Brain.Add(converted);
            
            return "";
        }

        public static string Substract()
        {
            Console.WriteLine("minus");
            Console.Write("number: ");
            var n = Console.ReadLine()?.Trim();
            double.TryParse(n, out var converted);

            Brain.Substract(converted);
            
            return "";
        }
        
        public static string Divide()
        {
            Console.WriteLine("divided by");
            Console.Write("number: ");
            var n = Console.ReadLine()?.Trim();
            double.TryParse(n, out var converted);
            if (converted == 0)
            {
                Console.WriteLine("Can't divide by 0");
                return "";
            }
            
            Brain.Divide(converted);
            
                return ""; 
            
            
        }
        
        public static string Multiply()
        {
            Console.WriteLine("multiplied by");
            Console.Write("number: ");
            var n = Console.ReadLine()?.Trim();
            double.TryParse(n, out var converted);

            //Brain.ApplyCustomFunction(CustomMultiply, converted);
            Brain.ApplyCustomFunction((a, b) => a * b, converted);
            return "";
        }

        public static double CustomMultiply(double a, double b)
        {
            return a * b;
        }
        
        public static string Power()
        {
            Console.WriteLine("power");
            Console.Write("number: ");
            var n = Console.ReadLine()?.Trim();
            double.TryParse(n, out var converted);

            Brain.Power(converted);
            
            return "";
        }

        public static string SubmenuUnary()
        {
            var menu = new Menu(ReturnCurrentDisplayValue,"Unary", EMenuLevel.First);
            menu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("Negate", "Negate", Negation),
                new MenuItem("Sqrt", "Sqrt", Sqrt),
                new MenuItem("Square", "Square", Square),
                new MenuItem("Abs", "Absolute Value", Absolute),
            });
            var res = menu.Run();
            return res;
        }
        
        public static string Negation()
        {
            Brain.Negation();
            
            return "";
        }
        
        public static string Sqrt()
        {

            Brain.Sqrt();
            
            return "";
        }
        
        public static string Square()
        {

            Brain.Square();
            
            return "";
        }
        
        public static string Absolute()
        {
            Brain.Absolute();
            
            return "";
        }
    }
}
