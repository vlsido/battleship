﻿namespace BattleShipBrain
{
    public enum EShipTouchRule
    {
        Touch,
        CornerTouch,
        NoTouch
    }
}