﻿using System;
using System.Diagnostics;

namespace BattleShipBrain
{
    public struct BoardSquareState
    {
        public bool IsShip { get; set; }
        public bool IsBomb { get; set; }
        public bool IsUnderAttack { get; set; }
        
        public bool IsPlaced { get; set; }
        public bool IsDestroyed { get; set; }
        

        public override string ToString()
        {
            switch (IsPlaced)
            {
                case (true):
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("[*]");
                    Console.ResetColor();
                    return "";
            }
            
            switch (IsDestroyed)
            {
                case (true):
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("[X]");
                    Console.ResetColor();
                    return "";
            }
            
            switch (IsShip, IsBomb, IsUnderAttack)
            {
                case (false, false, false):
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("[?]");
                    Console.ResetColor();
                    return "";
                case (false, true, false):
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("[O]");
                    Console.ResetColor();
                    return "";
                case (true, false, false):
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write("[8]");
                    Console.ResetColor();
                    return "";
                case (true, true, false):
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("[X]"
                        );
                    Console.ResetColor();
                    return "";
                case (false, false, true):
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("[?]");
                    Console.ResetColor();
                    return "";
                case (false, true, true):
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("[O]");
                    Console.ResetColor();
                    return "";
                case (true, false, true):
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("[8]");
                    Console.ResetColor();
                    return "";
                case (true, true, true):
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("[X]");
                    Console.ResetColor();
                    return "";
            }

            
        }
    }
}