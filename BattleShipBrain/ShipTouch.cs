﻿namespace BattleShipBrain
{
    public class ShipTouch
    {
        
        public GameConfig BoatsTouch(GameConfig touchRule)
        {
            if (touchRule.EShipTouchRule == EShipTouchRule.Touch)
            {
                touchRule.EShipTouchRule = EShipTouchRule.CornerTouch;
            }
            else if (touchRule.EShipTouchRule == EShipTouchRule.CornerTouch)
            {
                touchRule.EShipTouchRule = EShipTouchRule.NoTouch;
            }
            else if (touchRule.EShipTouchRule == EShipTouchRule.NoTouch)
            {
                touchRule.EShipTouchRule = EShipTouchRule.Touch;
            }


            return touchRule;
        }
    }
}