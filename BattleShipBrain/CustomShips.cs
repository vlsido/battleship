﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleShipBrain
{
    public class CustomShips
    {
        public GameConfig EditShips(GameConfig boardAConfig, GameConfig boardBConfig)
        {
            var boardChoice = boardAConfig;
            var choiceDone = false;
            Console.WriteLine("Choose board");
            Console.WriteLine("A) Board A");
            Console.WriteLine("B) Board B");
            do
            {
                var userInput = Console.ReadKey();
                switch (userInput.Key)
                {
                    case ConsoleKey.A:
                        boardChoice = boardAConfig;
                        choiceDone = true;
                        break;
                    case ConsoleKey.B:
                        boardChoice = boardBConfig;
                        choiceDone = true;
                        break;
                }
            } while (!choiceDone);
            

            var shipsDisplayed = true;
            do
            {
                List<int> ships = new List<int>();
            for (var i = 0; i < boardChoice.ShipConfigs.Count; i++)
            {
                Console.Write($"{i + 1}) ");
                Console.WriteLine(boardChoice.ShipConfigs.ElementAt(i).Name);
                ships.Add(i + 1);
            }
            Console.WriteLine("E) Exit");
            var done = false;
            do
            {
                var key = Console.ReadKey();
                Console.WriteLine("E) Exit");
                if (key.Key == ConsoleKey.E)
                {
                    return boardChoice;
                }
                int keyInt;
                // We check input for a Digit
                if (char.IsDigit(key.KeyChar))
                {
                    keyInt = int.Parse(key.KeyChar.ToString()); // use Parse if it's a Digit
                }
                else
                {
                    keyInt = 0;  // Else we assign a default value
                }
                for (var i = 0; i < boardChoice.ShipConfigs.Count; i++)
                {
                    if (keyInt == ships.ElementAt(i))
                    {
                        
                        do
                        {
                            int ySizeConverted;
                            int xSizeConverted;
                            int quantityConverted;
                            Console.WriteLine("====================================");
                            Console.WriteLine("|===" + boardChoice.ShipConfigs.ElementAt(i).Name + "===|");
                            Console.WriteLine("N) Name: " + boardChoice.ShipConfigs.ElementAt(i).Name);
                            Console.WriteLine("S) Size" + " " + "X: " + boardChoice.ShipConfigs.ElementAt(i).ShipSizeX +
                                              " " + "Y: " + boardChoice.ShipConfigs.ElementAt(i).ShipSizeY);
                            Console.WriteLine("Q) Quantity: " + boardChoice.ShipConfigs.ElementAt(i).Quantity);
                            Console.WriteLine("R) Remove");
                            Console.WriteLine("E) Exit");
                            var keyInfo = Console.ReadKey();
                            switch (keyInfo.Key)
                            {
                                case ConsoleKey.N:
                                    Console.WriteLine("New name: ");
                                    var name = Console.ReadLine();
                                    boardChoice.ShipConfigs.ElementAt(i).Name = name;
                                    break;
                                case ConsoleKey.S:
                                    Console.WriteLine("New ship size X: ");
                                    var xSize = Console.ReadLine();
                                    int.TryParse(xSize, out xSizeConverted);
                                    boardChoice.ShipConfigs.ElementAt(i).ShipSizeX = xSizeConverted;
                                    Console.WriteLine("New ship size Y: ");
                                    var ySize = Console.ReadLine();
                                    int.TryParse(ySize, out ySizeConverted);
                                    boardChoice.ShipConfigs.ElementAt(i).ShipSizeY = ySizeConverted;
                                    break;
                                case ConsoleKey.Q:
                                    Console.WriteLine("New quantity: ");
                                    var quantity = Console.ReadLine();
                                    int.TryParse(quantity, out quantityConverted);
                                    boardChoice.ShipConfigs.ElementAt(i).Quantity = quantityConverted;
                                    break;
                                case ConsoleKey.R:
                                    boardChoice.ShipConfigs.RemoveAt(i);
                                    done = true;
                                    break;
                                case ConsoleKey.E:
                                    done = true;
                                    break;
                            }
                        } while (!done);
                        
                        done = true;
                    }
                }
            } while (!done);
            
            } while (shipsDisplayed);
            
            return boardChoice;
        }
        
        public GameConfig AddShip(GameConfig boardAConfig, GameConfig boardBConfig)
        {
            var boardChoice = boardAConfig;
            var choiceDone = false;
            Console.WriteLine("Choose board");
            Console.WriteLine("A) Board A");
            Console.WriteLine("B) Board B");
            do
            {
                var userInput = Console.ReadKey();
                switch (userInput.Key)
                {
                    case ConsoleKey.A:
                        boardChoice = boardAConfig;
                        choiceDone = true;
                        break;
                    case ConsoleKey.B:
                        boardChoice = boardBConfig;
                        choiceDone = true;
                        break;
                }
            } while (!choiceDone);
            var done = false;
            var name = "";
            int quantityConverted = 0;
            int ySizeConverted = 0;
            int xSizeConverted = 0;
            do
            {
                Console.WriteLine("N) Name" + " " + name);
                Console.WriteLine("S) Size" + " " + "Y: " + ySizeConverted + " X: " + xSizeConverted);
                Console.WriteLine("Q) Quantity" + " " + quantityConverted);
                Console.WriteLine("A) Add");
                Console.WriteLine("E) Exit");
                var keyInfo = Console.ReadKey();
                switch (keyInfo.Key)
                {
                    case ConsoleKey.N:
                        Console.WriteLine("Enter your ship's name: ");
                        name = Console.ReadLine();
                        break;
                    case ConsoleKey.S:
                        Console.WriteLine("Enter ship size X: ");
                        var xSize = Console.ReadLine();
                        int.TryParse(xSize, out xSizeConverted);
                        Console.WriteLine("Enter ship size Y: ");
                        var ySize = Console.ReadLine();
                        int.TryParse(ySize, out ySizeConverted);
                        break;
                    case ConsoleKey.Q:
                        Console.WriteLine("Enter quantity: ");
                        var quantity = Console.ReadLine();
                        int.TryParse(quantity, out quantityConverted);
                        break;
                    case ConsoleKey.A:
                        if (name != "" && quantityConverted != 0 && ySizeConverted != 0 && xSizeConverted != 0)
                        {
                            boardChoice.ShipConfigs.Add(new ShipConfig()
                            {

                                Name = name,
                                Quantity = quantityConverted,
                                ShipSizeY = ySizeConverted,
                                ShipSizeX = xSizeConverted
                            });
                        }
                        else
                        {
                            Console.WriteLine("Something is not set");
                        }
                        new ShipConfig()
                                {
                                    Name = name,
                                    Quantity = quantityConverted,
                                    ShipSizeY = ySizeConverted,
                                    ShipSizeX = xSizeConverted
                                };
                                done = true;
                        break;
                    case ConsoleKey.E:
                        done = true;
                        break;
                }
            } while (!done);
            
            return boardChoice;
        }
    }
}