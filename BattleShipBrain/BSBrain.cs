﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BattleShipBrain
{
    public class BSBrain
    {
        private int _currentPlayerNo = 0;

        private GameBoard[] GameBoards = new GameBoard[4];

        private readonly Random _rnd = new Random();

        public BSBrain(GameConfig boardAConfig, GameConfig boardBConfig)
        {
            GameBoards[0] = new GameBoard();
            GameBoards[1] = new GameBoard();
            GameBoards[2] = new GameBoard();
            GameBoards[3] = new GameBoard();
            GameBoards[0].Board = new BoardSquareState[boardAConfig.BoardSizeX, boardAConfig.BoardSizeY];
            GameBoards[1].Board = new BoardSquareState[boardAConfig.BoardSizeX, boardAConfig.BoardSizeY];
            GameBoards[2].Board = new BoardSquareState[boardBConfig.BoardSizeX, boardBConfig.BoardSizeY];
            GameBoards[3].Board = new BoardSquareState[boardBConfig.BoardSizeX, boardBConfig.BoardSizeY];
            
            // Random map
            // for (var x = 0; x < config.BoardSizeX; x++)
            // {
            //     for (var y = 0; y < config.BoardSizeY; y++)
            //     {
            //         GameBoards[0].Board[x, y] = new BoardSquareState
            //         {
            //             IsBomb = false,
            //             IsShip = _rnd.Next(0, 2) != 0
            //         };
            //         GameBoards[1].Board[x, y] = new BoardSquareState
            //         {
            //             IsBomb = false,
            //             IsShip = _rnd.Next(0, 2) != 0
            //         };
            //         GameBoards[2].Board[x, y] = new BoardSquareState
            //         {
            //             IsBomb = false,
            //             IsShip = false
            //         };
            //         GameBoards[3].Board[x, y] = new BoardSquareState
            //         {
            //             IsBomb = false,
            //             IsShip = false
            //         };
            //     }
            // }
            
            for (var x = 0; x < boardAConfig.BoardSizeX; x++)
            {
                for (var y = 0; y < boardAConfig.BoardSizeY; y++)
                {
                    GameBoards[0].Board[x, y] = new BoardSquareState
                    {
                        IsBomb = false,
                        IsShip = false
                    };
                    GameBoards[1].Board[x, y] = new BoardSquareState
                    {
                        IsBomb = false,
                        IsShip = false
                    };
                }
            }
            
            for (var x = 0; x < boardBConfig.BoardSizeX; x++)
            {
                for (var y = 0; y < boardBConfig.BoardSizeY; y++)
                {
                    GameBoards[2].Board[x, y] = new BoardSquareState
                    {
                        IsBomb = false,
                        IsShip = false
                    };
                    GameBoards[3].Board[x, y] = new BoardSquareState
                    {
                        IsBomb = false,
                        IsShip = false
                    };
                }
            }
        }

        public BoardSquareState[,] GetBoard(int playerNo)
        {
            return CopyOfBoard(GameBoards[playerNo].Board);
        }

        private BoardSquareState[,] CopyOfBoard(BoardSquareState[,] board)
        {
            var res = new BoardSquareState[board.GetLength(0), board.GetLength(1)];
            for (var x = 0; x < board.GetLength(0); x++)
            {
                for (var y = 0; y < board.GetLength(1); y++)
                {
                    
                        res[x, y] = board[x, y];
                    
                }
                
            }

            return res;
        }

        public string getBrainJson()
        {
            var jsonOptions = new JsonSerializerOptions()
            {
                WriteIndented = true
            };
            
            var dto = new SaveGameDTO();
            var jsonStr = JsonSerializer.Serialize(dto, jsonOptions);
            return jsonStr;
        }

        public void restoreBrainFromJson(string json)
        {
            
        }
    }
}