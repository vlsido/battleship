﻿using System;

namespace BattleShipBrain
{
    public class BoardSize
    {
        public GameConfig BoardsSize(GameConfig boardSize)
        {
            Console.WriteLine("X size of a board: ");
            var xSize = Console.ReadLine();
            Console.WriteLine("Y size of a board: ");
            var ySize = Console.ReadLine();
            int.TryParse(xSize, out var xSizeConverted);
            int.TryParse(ySize, out var ySizeConverted);

            boardSize.BoardSizeX = xSizeConverted;
            boardSize.BoardSizeY = ySizeConverted;

            return boardSize;
        }
    }
}