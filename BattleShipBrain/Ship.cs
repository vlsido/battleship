﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

namespace BattleShipBrain
{
    public class Ship
    {
        public string Name { get; private set; }

        private readonly List<Coordinate> _coordinates = new List<Coordinate>();

        public Ship(string name, Coordinate position, int length, int width)
        {
            Name = name;
            for (var x = position.X; x < position.X + length; x++)
            {
                for (var y = position.Y; y < position.Y + width; y++)
                {
                    _coordinates.Add(new Coordinate(){X = x, Y = y});
                }
            }
        }
        
        
        
        public int GetShipSize() => _coordinates.Count;

        public int GetShipDamageCount(BoardSquareState[,] board) => 
            // count all the items that match the predicate
            _coordinates.Count(coordinate =>
                board[coordinate.X, coordinate.Y].IsBomb);
        
        public bool IsShipSunk(BoardSquareState[,] board) => 
            // returns true when all the items in the list match predicate
            _coordinates.All(coordinate => board[coordinate.X, coordinate.Y].IsBomb);

        
        public List<Coordinate> GetShipCoordinates()
        {
            return _coordinates;
        }
        
    }
}