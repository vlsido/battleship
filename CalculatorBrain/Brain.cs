﻿using System;

namespace CalculatorBrain
{
    public class Brain
    {
        public Double CurrentValue { get; private set; } = 0;

        public double Add(double value)
        {
            CurrentValue += value;
            return CurrentValue;
        }

        public void Substract(double value)
        {
            CurrentValue -= value;
        }

        public void Divide(double value)
        {
            CurrentValue /= value;
        }

        public void Power(double value)
        {
            CurrentValue = Math.Pow(CurrentValue, value);
        }

        public void Negation()
        {
            CurrentValue = (double) decimal.Negate((decimal) CurrentValue);
        }

        public void Sqrt()
        {
            if (CurrentValue > 0)
            {
                CurrentValue = Math.Sqrt(CurrentValue);
            }
            else if (CurrentValue < 0)
            {
                Console.WriteLine("Can't square root negative number!");
            }
        }

        public void Square()
        {
            CurrentValue *= CurrentValue;
        }

        public void Absolute()
        {
            CurrentValue = Math.Abs(CurrentValue);
        }

        public void Clear()
        {
            CurrentValue = 0;
        }

        public void SetValue(double value) => CurrentValue = value;

        public double ApplyCustomFunction(Func<double, double, double> funcToApply, double value)
        {
            CurrentValue = funcToApply(CurrentValue, value);
            return CurrentValue;
        }
    }
}