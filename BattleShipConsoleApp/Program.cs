﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mime;
using System.Net.Sockets;
using System.Reflection.Metadata;
using System.Text.Json;
using BattleShipBrain;
using BattleShipConsoleUI;
using MenuSystem;

namespace BattleShipConsoleApp
{
    internal class Program
    {
        public static string _basePath = default!;
        private static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Hello BS game!");
            
            var boardAConfig = new GameConfig();
            var boardBConfig = new GameConfig();
            var brain = new BSBrain(boardAConfig, boardBConfig);
            
            var fileNameSaved = _basePath + System.IO.Path.DirectorySeparatorChar + "SavedGames" +
                                System.IO.Path.DirectorySeparatorChar + "game.json";
            Console.WriteLine(brain.getBrainJson());


            var mainMenu = new Menu("BattleShip", EMenuLevel.Root);
            mainMenu.AddMenuItems(new List<MenuItem>()
            {
                new("N", "New game", SubmenuNewGame),
                new("L", "Load game", SubmenuLoadGame)
            });
            

            mainMenu.Run();


            BSConsoleUI.DrawBoard(brain.GetBoard(0));

            brain.GetBoard(0)[0, 0].IsBomb = true;
            brain.GetBoard(0)[0, 0].IsShip = false;
            Console.WriteLine("---------------------------");

            BSConsoleUI.DrawBoard(brain.GetBoard(0));
            
        }

        public static string SaveGame(string[] args, GameConfig boardAConfig, GameConfig boardBConfig, BSBrain brain)
        {
            
            _basePath = args.Length == 1 ? args[0] : System.IO.Directory.GetCurrentDirectory();
            Console.WriteLine($"Base path: {_basePath}");
            
            
            
            var conf = new GameConfig();
            

            
            var jsonOptions = new JsonSerializerOptions()
            {
                WriteIndented = true
            };
            var fileNameStandardConfig = _basePath + System.IO.Path.DirectorySeparatorChar + "Configs" + System.IO.Path.DirectorySeparatorChar + "standard.json";
            
            var confJsonStr = JsonSerializer.Serialize(conf, jsonOptions);
            
            if (!System.IO.File.Exists(fileNameStandardConfig))
            {
                System.IO.File.WriteAllText(fileNameStandardConfig, confJsonStr);
            }
            
            if (System.IO.File.Exists(fileNameStandardConfig))
            {
                Console.WriteLine("Loading config...");
                var confText = System.IO.File.ReadAllText(fileNameStandardConfig);
                conf = JsonSerializer.Deserialize<GameConfig>(confText);
                Console.WriteLine(conf);
            }
            var fileNameSaved = _basePath + System.IO.Path.DirectorySeparatorChar + "SavedGames" +
                                System.IO.Path.DirectorySeparatorChar + "game.json";
            Console.WriteLine(brain.getBrainJson());

            return "";
        }

        public static string SubmenuNewGame()
        {
            var boardAConfig = new GameConfig();
            var boardBConfig = new GameConfig();
            var brain = new BSBrain(boardAConfig, boardBConfig);
            
            var menu = new Menu("New game", EMenuLevel.First);
            menu.AddMenuItems(new List<MenuItem>()
            {
                new("O", "Options", SubmenuOptions),
                new("S", "Start", StartGameFunction)
            });
            var res = menu.Run();
            return res;
            
            string StartGameFunction()
            {
                StartGame(brain, boardAConfig, boardBConfig);
                var res = "";
                return res;
            }
        }

        public static string SubmenuLoadGame()
        {
            
            var menu = new Menu("Load game", EMenuLevel.First);
            menu.AddMenuItems(new List<MenuItem>()
            {
                new("S", "Saved Games", SavedGames),
            });
            var res = menu.Run();
            return res;
            
            
        }

        public static string SubmenuOptions()
        {
            var boardAConfig = new GameConfig();
            var boardBConfig = new GameConfig();
            var brain = new BSBrain(boardAConfig, boardBConfig);
            var boardSize = new BoardSize();
            var touchRule = new ShipTouch();
            var customShips = new CustomShips();
            Console.WriteLine($"Board size - X: {boardAConfig.BoardSizeX}, Y: {boardAConfig.BoardSizeY}");
            Console.WriteLine($"Touch rule - {boardAConfig.EShipTouchRule}");
            var menu = new Menu("Options", EMenuLevel.SecondOrMore);
            menu.AddMenuItems(new List<MenuItem>()
            {
                new("O", "Change board size", BoardSizeFunction),
                new("T", "Change touch rule", BoatsTouchRule),
                new("C", "Customize ships", CustomizeShipsFunction),
                new("S", "Start game", StartGameFunction)
            });
            var res = menu.Run();
            return res;

            string BoardSizeFunction()
            {
                boardSize.BoardsSize(boardAConfig);
                boardBConfig.BoardSizeX = boardAConfig.BoardSizeX;
                boardBConfig.BoardSizeY = boardAConfig.BoardSizeY;
                var res = "";
                return res;
            }
            
            string BoatsTouchRule()
            {
                touchRule.BoatsTouch(boardAConfig);
                Console.WriteLine(boardAConfig.EShipTouchRule);
                var res = "";
                return res;
            }
            
            string CustomizeShipsFunction()
            {
                var menu = new Menu("Ships", EMenuLevel.SecondOrMore);
                menu.AddMenuItems(new List<MenuItem>()
                    {
                        new("C", "Customize ships", EditShipsFunction),
                        new("A", "Add ships", AddShipFunction),
                    });
                menu.Run();
                var res = "";
                return res;

                string EditShipsFunction()
                {
                    customShips.EditShips(boardAConfig, boardBConfig);
                    var res = "";
                    return res;
                }

                string AddShipFunction()
                {
                    customShips.AddShip(boardAConfig, boardBConfig);
                    var res = "";
                    return res;
                }
            }
            
            string StartGameFunction()
            {
                StartGame(brain, boardAConfig, boardBConfig);
                var res = "";
                return res;
            }
            
            
        }

        public static Func<string>? StartGame(BSBrain brain, GameConfig boardAConfig, GameConfig boardBConfig)
        {
            var boardAConfigOriginal = boardAConfig;
            var boardBConfigOriginal = boardBConfig;
            boardAConfig = boardAConfigOriginal;
            boardBConfig = boardBConfigOriginal;
            var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var userChoice = "";
            Console.WriteLine("Enjoy your game!");



            // boardAConfig.EShipTouchRule = EShipTouchRule.NoTouch;

            
            var _rnd = new Random();
            Dictionary<string, Ship> myDict = new();
            Dictionary<string, Ship> boardBDict = new();
            List<List<Coordinate>> coords = new();
            List<List<Coordinate>> coordsB = new();

            var boardA = brain.GetBoard(0);

            Console.WriteLine("");
            brain.GetBoard(0)[0, 0].IsBomb = false;
            brain.GetBoard(0)[0, 0].IsShip = false;

            

            var boardAHidden = brain.GetBoard(2);

            Console.WriteLine("");

            var boardB = brain.GetBoard(1);

            var elementsA = boardAConfig.ShipConfigs.Count - 1;
            var elementsB = boardBConfig.ShipConfigs.Count - 1;

            Console.WriteLine("");
            brain.GetBoard(1)[0, 0].IsBomb = false;
            brain.GetBoard(1)[0, 0].IsShip = false;

            var boardBHidden = brain.GetBoard(3);
            brain.GetBoard(3)[0, 0].IsBomb = false;
            brain.GetBoard(3)[0, 0].IsShip = false;

            

            var count = 0;
            var countB = 0;


            var shipAxisX = 0;
            var shipAxisY = 0;

            var aWon = false;
            var bWon = false;

            for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
            {
                for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                {
                    boardA[shipAxisX + a, shipAxisY + b].IsPlaced = true;
                }
            }
            var shipsPlaced = false;
            var bShipsPlaced = false;
            do
            {
                Console.Clear();
                Console.WriteLine("Place your ships now!");
                Console.Write("   ");
                for (var i = 0; i < boardAConfig.BoardSizeX; i++)
                {
                    Console.Write(alphabet[i] + " ");
                    if (i + 1 == boardAConfig.BoardSizeX) Console.WriteLine("");
                }
            
                BSConsoleUI.DrawBoard(boardA);
                
                Console.WriteLine("\nYour ships:");
                for (var i = 0; i < boardAConfig.ShipConfigs.Count; i++)
                {
                    Console.WriteLine(boardAConfig.ShipConfigs.ElementAt(i).ShipSizeX + "x" +
                                      boardAConfig.ShipConfigs.ElementAt(i).ShipSizeY + " " +
                                      boardAConfig.ShipConfigs.ElementAt(i).Name + ": " +
                                      boardAConfig.ShipConfigs.ElementAt(i).Quantity);
                }
                
                elementsA = boardAConfig.ShipConfigs.Count - 1;
                for (var i = elementsA; i >= 0; i--)
                    if (boardAConfig.ShipConfigs.ElementAt(i).Quantity == 0)
                        if (elementsA > 0)
                            elementsA -= 1;
                
                var takenPlace = false;
                var keyInfo = Console.ReadKey();
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:

                        for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
                        {
                            for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                            {
                                boardA[shipAxisX + a, shipAxisY + b].IsPlaced = false;
                            }
                        }

                        if (shipAxisY - 1 < 0)
                            break;
                        else
                            shipAxisY -= 1;
                        for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
                        {
                            for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                            {
                                boardA[shipAxisX + a, shipAxisY + b].IsPlaced = true;
                            }
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
                        {
                            for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                            {
                                boardA[shipAxisX + a, shipAxisY + b].IsPlaced = false;
                            }
                        }
            
                        if (shipAxisY + 1 + boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY > boardAConfig.BoardSizeY)
                            break;
                        else
                            shipAxisY += 1;
                        for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
                        {
                            for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                            {
                                boardA[shipAxisX + a, shipAxisY + b].IsPlaced = true;
                            }
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
                        {
                            for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                            {
                                boardA[shipAxisX + a, shipAxisY + b].IsPlaced = false;
                            }
                        }
            
                        if (shipAxisX + 1 + boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX > boardAConfig.BoardSizeX)
                            break;
                        else
                            shipAxisX += 1;
                        for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
                        {
                            for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                            {
                                boardA[shipAxisX + a, shipAxisY + b].IsPlaced = true;
                            }
                        }
                        break;
                    case ConsoleKey.LeftArrow:
                        for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
                        {
                            for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                            {
                                boardA[shipAxisX + a, shipAxisY + b].IsPlaced = false;
                            }
                        }
            
                        if (shipAxisX - 1 < 0)
                            break;
                        else
                            shipAxisX -= 1;
                        for (var a = 0; a < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; a++)
                        {
                            for (var b = 0; b < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; b++)
                            {
                                boardA[shipAxisX + a, shipAxisY + b].IsPlaced = true;
                            }
                        }
                        break;
                    case ConsoleKey.R:
                        (shipAxisX, shipAxisY) = (shipAxisY, shipAxisX);
                        if (shipAxisX + boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX <= boardAConfig.BoardSizeY && shipAxisY + boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY <= boardAConfig.BoardSizeX)
                        {
                            (shipAxisX, shipAxisY) = (shipAxisY, shipAxisX);
                            for (var x = 0; x < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; x++)
                            {
                                for (var y = 0; y < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; y++)
                                {
                                    if (boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX >
                                        boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY)
                                    {
                                        boardA[shipAxisX + x, shipAxisY + y].IsPlaced = false;
                                    }
                                    else if (boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX <
                                             boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY)
                                    {
                                        boardA[shipAxisX + x,
                                                shipAxisY + y]
                                            .IsPlaced = false;
                                    }
                                }
                            }
                            for (var x = 0; x < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX; x++)
                            {
                                for (var y = 0; y < boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY; y++)
                                {
                                    if (boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX >
                                        boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY)
                                    {
                                        boardA[shipAxisX + y, shipAxisY + x].IsPlaced = true;
                                    }
                                    else if (boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX <
                                             boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY)
                                    {
                                        boardA[shipAxisX + y, shipAxisY + x].IsPlaced = true;
                                    }
                                }
                            }
                            (boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX, boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY) = (boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY, boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX);
                        }
                        else
                        {
                            (shipAxisX, shipAxisY) = (shipAxisY, shipAxisX);
                        }
                        break;
                    
                    case ConsoleKey.Enter:
            
                        if (boardAConfig.EShipTouchRule == EShipTouchRule.Touch)
                        {
                            var name = boardAConfig.ShipConfigs.ElementAt(elementsA).Name;
                            var ship = new Coordinate();
                            ship.X = shipAxisX;
                            ship.Y = shipAxisY;
                            Ship ships = new(name, ship, boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX,
                                boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY);
                            var alreadyExist = myDict.ContainsKey(name + "[" + count + "]");
            
                            if (!alreadyExist) myDict.Add(name + "[" + count + "]", ships);
            
                            alreadyExist = coords.Contains(myDict.ElementAt(count).Value.GetShipCoordinates());
                            if (!alreadyExist) coords.Add(myDict.ElementAt(count).Value.GetShipCoordinates());
            
                            for (var a = count; a < coords.Count; a++)
                            {
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (boardA[coords[a][b].X, coords[a][b].Y].IsShip)
                                        takenPlace = true;
            
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (boardA[coords[a][b].X, coords[a][b].Y].IsPlaced && takenPlace == false)
                                    {
                                        boardA[coords[a][b].X, coords[a][b].Y].IsPlaced = false;
                                        boardA[coords[a][b].X, coords[a][b].Y].IsShip = true;
                                    }
                            }
            
                            if (takenPlace == true)
                            {
                                coords.Remove(myDict.ElementAt(count).Value.GetShipCoordinates());
                                myDict.Remove(name + "[" + count + "]");
                            }
            
                            if (takenPlace == false)
                            {
                                boardAConfig.ShipConfigs.ElementAt(elementsA).Quantity -= 1;
                                if (boardAConfig.ShipConfigs.ElementAt(0).Quantity == 0) shipsPlaced = true;
            
                                count += 1;
                            }
            
                            takenPlace = false;
                        }
            
                        else if (boardAConfig.EShipTouchRule == EShipTouchRule.CornerTouch)
                        {
                            var name = boardAConfig.ShipConfigs.ElementAt(elementsA).Name;
                            var ship = new Coordinate();
                            ship.X = shipAxisX;
                            ship.Y = shipAxisY;
                            Ship ships = new(name, ship, boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX,
                                boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY);
                            var alreadyExist = myDict.ContainsKey(name + "[" + count + "]");
            
                            if (!alreadyExist) myDict.Add(name + "[" + count + "]", ships);
            
                            alreadyExist = coords.Contains(myDict.ElementAt(count).Value.GetShipCoordinates());
                            if (!alreadyExist) coords.Add(myDict.ElementAt(count).Value.GetShipCoordinates());
            
                            for (var a = count; a < coords.Count; a++)
                            {
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (coords[a][b].X + 1 <= boardAConfig.BoardSizeX - 1 && coords[a][b].X - 1 >= 0)
                                        if (boardA[coords[a][b].X, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X + 1, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X - 1, coords[a][b].Y].IsShip)
                                            takenPlace = true;
            
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (coords[a][b].Y + 1 <= boardAConfig.BoardSizeY - 1 && coords[a][b].Y - 1 >= 0)
                                        if (boardA[coords[a][b].X, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X, coords[a][b].Y + 1].IsShip
                                            || boardA[coords[a][b].X, coords[a][b].Y - 1].IsShip)
                                            takenPlace = true;
            
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (boardA[coords[a][b].X, coords[a][b].Y].IsPlaced && takenPlace == false)
                                    {
                                        boardA[coords[a][b].X, coords[a][b].Y].IsPlaced = false;
                                        boardA[coords[a][b].X, coords[a][b].Y].IsShip = true;
                                    }
                            }
            
                            if (takenPlace == true)
                            {
                                coords.Remove(myDict.ElementAt(count).Value.GetShipCoordinates());
                                myDict.Remove(name + "[" + count + "]");
                            }
            
                            if (takenPlace == false)
                            {
                                boardAConfig.ShipConfigs.ElementAt(elementsA).Quantity -= 1;
                                if (boardAConfig.ShipConfigs.ElementAt(0).Quantity == 0) shipsPlaced = true;
            
                                count += 1;
                            }
            
                            takenPlace = false;
                        }
                        else if (boardAConfig.EShipTouchRule == EShipTouchRule.NoTouch)
                        {
                            var name = boardAConfig.ShipConfigs.ElementAt(elementsA).Name;
                            var ship = new Coordinate();
                            ship.X = shipAxisX;
                            ship.Y = shipAxisY;
                            Ship ships = new(name, ship, boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeX,
                                boardAConfig.ShipConfigs.ElementAt(elementsA).ShipSizeY);
                            var alreadyExist = myDict.ContainsKey(name + "[" + count + "]");
            
                            if (!alreadyExist) myDict.Add(name + "[" + count + "]", ships);
            
                            alreadyExist = coords.Contains(myDict.ElementAt(count).Value.GetShipCoordinates());
                            if (!alreadyExist) coords.Add(myDict.ElementAt(count).Value.GetShipCoordinates());
            
                            for (var a = count; a < coords.Count; a++)
                            {
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (coords[a][b].X + 1 <= boardAConfig.BoardSizeX - 1 && coords[a][b].X - 1 >= 0)
                                        if (boardA[coords[a][b].X, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X + 1, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X - 1, coords[a][b].Y].IsShip)
                                            takenPlace = true;
            
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (coords[a][b].Y + 1 <= boardAConfig.BoardSizeY - 1 && coords[a][b].Y - 1 >= 0)
                                        if (boardA[coords[a][b].X, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X, coords[a][b].Y + 1].IsShip
                                            || boardA[coords[a][b].X, coords[a][b].Y - 1].IsShip)
                                            takenPlace = true;
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (coords[a][b].X + 1 <= boardAConfig.BoardSizeX - 1 &&
                                        coords[a][b].Y + 1 <= boardAConfig.BoardSizeY - 1)
                                        if (boardA[coords[a][b].X, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X + 1, coords[a][b].Y + 1].IsShip)
                                            takenPlace = true;
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (coords[a][b].X - 1 >= 0 && coords[a][b].Y - 1 >= 0)
                                        if (boardA[coords[a][b].X, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X - 1, coords[a][b].Y - 1].IsShip)
                                            takenPlace = true;
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (coords[a][b].X + 1 <= boardAConfig.BoardSizeX - 1 && coords[a][b].Y - 1 >= 0)
                                        if (boardA[coords[a][b].X, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X + 1, coords[a][b].Y - 1].IsShip)
                                            takenPlace = true;
                                for (var b = 0; b < coords[a].Count; b++)
                                    if (coords[a][b].X - 1 >= 0 && coords[a][b].Y + 1 <= boardAConfig.BoardSizeY - 1)
                                        if (boardA[coords[a][b].X, coords[a][b].Y].IsShip
                                            || boardA[coords[a][b].X - 1, coords[a][b].Y + 1].IsShip)
                                            takenPlace = true;
            
            
                                for (var b = 0; b < coords[a].Count; b++)
            
                                    if (boardA[coords[a][b].X, coords[a][b].Y].IsPlaced && takenPlace == false)
                                    {
                                        boardA[coords[a][b].X, coords[a][b].Y].IsPlaced = false;
                                        boardA[coords[a][b].X, coords[a][b].Y].IsShip = true;
                                    }
                            }
            
            
                            if (takenPlace == true)
                            {
                                coords.Remove(myDict.ElementAt(count).Value.GetShipCoordinates());
                                myDict.Remove(name + "[" + count + "]");
                            }
            
                            if (takenPlace == false)
                            {
                                boardAConfig.ShipConfigs.ElementAt(elementsA).Quantity -= 1;
                                if (boardAConfig.ShipConfigs.ElementAt(0).Quantity == 0) shipsPlaced = true;
            
                                count += 1;
                            }
            
                            takenPlace = false;
                        }
                        break;
                    
                    case ConsoleKey.Escape:
                        Console.WriteLine("===PAUSE===");
                        Console.WriteLine("S) Save Game");
                        Console.WriteLine("R) Return");
                        Console.WriteLine("E) Exit to main menu");
                        var escapeKeyInfo = Console.ReadKey();
                        switch (escapeKeyInfo.Key)
                        {
                            case ConsoleKey.S:
                                break;
                            case ConsoleKey.R:
                                break;
                            // case ConsoleKey.E:
                            //     return;
                            //     break;
                        }
                        break;
                }
                
                
            } while (!shipsPlaced);

            
            if (shipsPlaced == true)
            {
                
                do
                {

                    elementsB = boardBConfig.ShipConfigs.Count - 1;
                    for (var i = elementsB; i >= 0; i--)
                        if (boardBConfig.ShipConfigs.ElementAt(i).Quantity == 0)
                            if (elementsB > 0)
                                elementsB -= 1;
                    var takenPlace = false;
                    if (boardAConfig.EShipTouchRule == EShipTouchRule.NoTouch)
                    {

                        var name = boardBConfig.ShipConfigs.ElementAt(elementsB).Name;
                        var ship = new Coordinate();
                        
                        var randomRotate = _rnd.Next(0, 2);
                        if (randomRotate == 1)
                        {
                            (boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeX, boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeY) = (boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeY, boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeX);
                            ship.X = _rnd.Next(0, (boardBConfig.BoardSizeX - boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeX) + 1);
                            ship.Y = _rnd.Next(0, (boardBConfig.BoardSizeY - boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeY) + 1);
                        }
                        else
                        {
                            ship.X = _rnd.Next(0, (boardBConfig.BoardSizeX - boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeX) + 1);
                            ship.Y = _rnd.Next(0, (boardBConfig.BoardSizeY - boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeY) + 1);
                        }
                        Ship ships = new(name, ship, boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeX,
                            boardBConfig.ShipConfigs.ElementAt(elementsB).ShipSizeY);
                        var alreadyExist = boardBDict.ContainsKey(name + "[" + countB + "]");

                        if (!alreadyExist) boardBDict.Add(name + "[" + countB + "]", ships);

                        alreadyExist = coordsB.Contains(boardBDict.ElementAt(countB).Value.GetShipCoordinates());
                        if (!alreadyExist) coordsB.Add(boardBDict.ElementAt(countB).Value.GetShipCoordinates());

                        for (var a = countB; a < coordsB.Count; a++)
                        {
                            for (var b = 0; b < coordsB[a].Count; b++)
                                if (coordsB[a][b].X + 1 <= boardAConfig.BoardSizeX - 1 && coordsB[a][b].X - 1 >= 0)
                                    if (boardB[coordsB[a][b].X, coordsB[a][b].Y].IsShip
                                        || boardB[coordsB[a][b].X + 1, coordsB[a][b].Y].IsShip
                                        || boardB[coordsB[a][b].X - 1, coordsB[a][b].Y].IsShip)
                                        takenPlace = true;

                            for (var b = 0; b < coordsB[a].Count; b++)
                                if (coordsB[a][b].Y + 1 <= boardAConfig.BoardSizeY - 1 && coordsB[a][b].Y - 1 >= 0)
                                    if (boardB[coordsB[a][b].X, coordsB[a][b].Y].IsShip
                                        || boardB[coordsB[a][b].X, coordsB[a][b].Y + 1].IsShip
                                        || boardB[coordsB[a][b].X, coordsB[a][b].Y - 1].IsShip)
                                        takenPlace = true;
                            for (var b = 0; b < coordsB[a].Count; b++)
                                if (coordsB[a][b].X + 1 <= boardAConfig.BoardSizeX - 1 &&
                                    coordsB[a][b].Y + 1 <= boardAConfig.BoardSizeY - 1)
                                    if (boardB[coordsB[a][b].X, coordsB[a][b].Y].IsShip
                                        || boardB[coordsB[a][b].X + 1, coordsB[a][b].Y + 1].IsShip)
                                        takenPlace = true;
                            for (var b = 0; b < coordsB[a].Count; b++)
                                if (coordsB[a][b].X - 1 >= 0 && coordsB[a][b].Y - 1 >= 0)
                                    if (boardB[coordsB[a][b].X, coordsB[a][b].Y].IsShip
                                        || boardB[coordsB[a][b].X - 1, coordsB[a][b].Y - 1].IsShip)
                                        takenPlace = true;
                            for (var b = 0; b < coordsB[a].Count; b++)
                                if (coordsB[a][b].X + 1 <= boardAConfig.BoardSizeX - 1 && coordsB[a][b].Y - 1 >= 0)
                                    if (boardA[coordsB[a][b].X, coordsB[a][b].Y].IsShip
                                        || boardB[coordsB[a][b].X + 1, coordsB[a][b].Y - 1].IsShip)
                                        takenPlace = true;
                            for (var b = 0; b < coordsB[a].Count; b++)
                                if (coordsB[a][b].X - 1 >= 0 && coordsB[a][b].Y + 1 <= boardAConfig.BoardSizeY - 1)
                                    if (boardB[coordsB[a][b].X, coordsB[a][b].Y].IsShip
                                        || boardB[coordsB[a][b].X - 1, coordsB[a][b].Y + 1].IsShip)
                                        takenPlace = true;


                            for (var b = 0; b < coordsB[a].Count; b++)

                                if (takenPlace == false)
                                {
                                    boardB[coordsB[a][b].X, coordsB[a][b].Y].IsShip = true;
                                }
                        }


                        if (takenPlace == true)
                        {
                            coordsB.Remove(boardBDict.ElementAt(countB).Value.GetShipCoordinates());
                            boardBDict.Remove(name + "[" + countB + "]");
                        }

                        if (takenPlace == false)
                        {
                            boardBConfig.ShipConfigs.ElementAt(elementsB).Quantity -= 1;
                            if (boardBConfig.ShipConfigs.ElementAt(0).Quantity == 0) bShipsPlaced = true;

                            
                            countB += 1;
                        }

                        takenPlace = false;
                    }
                    // BSConsoleUI.DrawBoard(boardB);

                } while (!bShipsPlaced);

            }
            
            List<Dictionary<int, Dictionary<int, int>>> hitSquares = new();
            
            int hitCount = 0;
            var missXSquare = 0;
            var missYSquare = 0;
            var activateCode = false;
            DictionaryEntry[] hitSquaresArray = new DictionaryEntry[hitSquares.Count];
            do
            {
                var gameOver = false;
                var activateFindShipsToDestroy = true;
                boardBHidden[0, 0].IsUnderAttack = true;
                var axisX = 0;
                var axisY = 0;
                var xSquare = 0;
                var ySquare = 0;
                var myDictCopy = myDict;
                do
                {
                   
                    var moveDone = false;
                    do
                    {
                        Console.Clear();
                        if (activateCode == true)
                        {
                        
                            if (hitSquares.Count == 1)
                            {
                                Console.WriteLine($"Player B has made 1 successful hit!");
                            }
                            else if (hitSquares.Count >= 0 && hitSquares.Count != 1)
                            {
                                Console.WriteLine($"Player B has made {hitSquares.Count} successful hits!");
                            }

                            if (hitSquares.Count > 0)
                            {
                                Console.WriteLine("Hit squares:");
                                for (var i = 0; i < hitSquares.Count; i++)
                                {
                                    foreach (var key in hitSquares[i].Keys)
                                    {
                                        Console.WriteLine($"X: {hitSquares[i][key].ElementAt(0).Key}, Y: {hitSquares[i][key].ElementAt(0).Value}");
                                    }
                                }
                            } 
                            Console.WriteLine($"Miss squares:\nX: {missXSquare} Y: {missYSquare} ");
                        }

                        
                        
                        for (var i = 0; i < boardAConfig.BoardSizeX; i++) Console.Write(alphabet[i] + " ");
                        Console.WriteLine("");
                        BSConsoleUI.DrawBoard(boardA);


                        Console.WriteLine("---------------------------");

                        for (var i = 0; i < boardBConfig.BoardSizeX; i++) Console.Write(alphabet[i] + " ");

                        Console.WriteLine("");
                        BSConsoleUI.DrawBoard(boardBHidden);
                        var keyInfo = Console.ReadKey();
                        switch (keyInfo.Key)
                        {
                            case ConsoleKey.UpArrow:
                                boardBHidden[axisX, axisY].IsUnderAttack = false;
                                if (axisY - 1 >= 0)
                                {
                                    axisY -= 1;
                                }

                                boardBHidden[axisX, axisY].IsUnderAttack = true;
                                break;
                            case ConsoleKey.DownArrow:
                                boardBHidden[axisX, axisY].IsUnderAttack = false;
                                if (axisY + 1 > boardAConfig.BoardSizeY - 1)
                                    break;
                                else
                                    axisY += 1;

                                boardBHidden[axisX, axisY].IsUnderAttack = true;
                                break;
                            
                            case ConsoleKey.RightArrow:
                                boardBHidden[axisX, axisY].IsUnderAttack = false;
                                if (axisX + 1 > boardAConfig.BoardSizeX - 1)
                                    break;
                                else
                                    axisX += 1;
                                boardBHidden[axisX, axisY].IsUnderAttack = true;
                                break;
                            
                            case ConsoleKey.LeftArrow:
                                boardBHidden[axisX, axisY].IsUnderAttack = false;
                                if (axisX - 1 >= 0)
                                {
                                    axisX -= 1;
                                }

                                boardBHidden[axisX, axisY].IsUnderAttack = true;
                                break;
                            
                            case ConsoleKey.Enter:
                                if (boardB[axisX, axisY].IsShip && boardB[axisX, axisY].IsBomb == false)
                                {
                                    boardB[axisX, axisY].IsBomb = true;
                                    boardBHidden[axisX, axisY].IsShip = true;
                                    boardBHidden[axisX, axisY].IsBomb = true;
                                }
                                else if (boardB[axisX, axisY].IsShip == false && boardB[axisX, axisY].IsBomb == false)
                                {
                                    boardB[axisX, axisY].IsBomb = true;
                                    boardBHidden[axisX, axisY].IsShip = false;
                                    boardBHidden[axisX, axisY].IsBomb = true;
                                    moveDone = true;
                                }
                                else if (boardB[axisX, axisY].IsShip && boardB[axisX, axisY].IsBomb)
                                {
                                    boardBHidden[axisX, axisY].IsShip = true;
                                    boardBHidden[axisX, axisY].IsBomb = true;
                                }
                                else if (boardB[axisX, axisY].IsShip == false && boardB[axisX, axisY].IsBomb)
                                {
                                    boardBHidden[axisX, axisY].IsShip = false;
                                    boardBHidden[axisX, axisY].IsBomb = true;
                                }

                                for (var i = 0; i < boardBDict.Count; i++)
                                {
                                    if (boardBDict.ElementAt(i).Value.IsShipSunk(boardB))
                                    {
                                        for (var b = 0; b < coordsB[i].Count; b++)
                                        {
                                            if (coordsB[i][b].X - 1 >= 0)
                                            {
                                                boardB[coordsB[i][b].X - 1, coordsB[i][b].Y].IsBomb = true;
                                                boardBHidden[coordsB[i][b].X - 1, coordsB[i][b].Y].IsBomb = true;
                                            }

                                            if (coordsB[i][b].X + 1 < boardBConfig.BoardSizeX)
                                            {
                                                boardB[coordsB[i][b].X + 1, coordsB[i][b].Y].IsBomb = true;
                                                boardBHidden[coordsB[i][b].X + 1, coordsB[i][b].Y].IsBomb = true;
                                            }

                                            if (coordsB[i][b].Y - 1 >= 0)
                                            {
                                                boardB[coordsB[i][b].X, coordsB[i][b].Y - 1].IsBomb = true;
                                                boardBHidden[coordsB[i][b].X, coordsB[i][b].Y - 1].IsBomb = true;
                                            }

                                            if (coordsB[i][b].Y + 1 < boardBConfig.BoardSizeY)
                                            {
                                                boardB[coordsB[i][b].X, coordsB[i][b].Y + 1].IsBomb = true;
                                                boardBHidden[coordsB[i][b].X, coordsB[i][b].Y + 1].IsBomb = true;
                                            }

                                            if (coordsB[i][b].X + 1 < boardBConfig.BoardSizeX &&
                                                coordsB[i][b].Y + 1 < boardBConfig.BoardSizeY)
                                            {
                                                boardB[coordsB[i][b].X + 1, coordsB[i][b].Y + 1].IsBomb = true;
                                                boardBHidden[coordsB[i][b].X + 1, coordsB[i][b].Y + 1].IsBomb = true;
                                            }

                                            if (coordsB[i][b].X + 1 < boardBConfig.BoardSizeX &&
                                                coordsB[i][b].Y - 1 >= 0)
                                            {
                                                boardB[coordsB[i][b].X + 1, coordsB[i][b].Y - 1].IsBomb = true;
                                                boardBHidden[coordsB[i][b].X + 1, coordsB[i][b].Y - 1].IsBomb = true;
                                            }

                                            if (coordsB[i][b].X - 1 >= 0 &&
                                                coordsB[i][b].Y + 1 < boardBConfig.BoardSizeY)
                                            {
                                                boardB[coordsB[i][b].X - 1, coordsB[i][b].Y + 1].IsBomb = true;
                                                boardBHidden[coordsB[i][b].X - 1, coordsB[i][b].Y + 1].IsBomb = true;
                                            }

                                            if (coordsB[i][b].X - 1 >= 0 && coordsB[i][b].Y - 1 >= 0)
                                            {
                                                boardB[coordsB[i][b].X - 1, coordsB[i][b].Y - 1].IsBomb = true;
                                                boardBHidden[coordsB[i][b].X - 1, coordsB[i][b].Y - 1].IsBomb = true;
                                            }
                                        }
                                    }
                                }
                                break;
                        }

                        if (moveDone == true)
                        {
                            hitSquares.Clear();
                        }
                    } while (!moveDone);



                    // Random player2 (computer) hit

                    var nextXPlusSquare = 1;
                    var nextXMinusSquare = 1;
                    var nextYPlusSquare = 1;
                    var nextYMinusSquare = 1;
                    var nextXPlusMove = 0;
                    var nextXMinusMove = 0;
                    var nextYPlusMove = 0;
                    var nextYMinusMove = 0;

                    var computerMoveDone = false;
                    var bombFound = false;
                    
                    
                    
                    
                    
                    if (activateFindShipsToDestroy == true)
                    {
                        for (var ySquareToCheck = 0; ySquareToCheck < boardAConfig.BoardSizeX; ySquareToCheck++)
                        {
                            for (var xSquareToCheck = 0; xSquareToCheck < boardAConfig.BoardSizeY; xSquareToCheck++)
                            {

                                if (boardAHidden[xSquareToCheck, ySquareToCheck].IsBomb &&
                                    boardAHidden[xSquareToCheck, ySquareToCheck].IsShip &&
                                    boardAHidden[xSquareToCheck, ySquareToCheck].IsDestroyed == false)
                                {
                                    bombFound = true;
                                    activateFindShipsToDestroy = false;
                                    xSquare = xSquareToCheck;
                                    ySquare = ySquareToCheck;
                                }

                                if (bombFound)
                                {
                                    break;
                                }
                            }

                            if (bombFound)
                            {
                                break;
                            }
                        }
                    }
                    
                        
                        do
                        {
                            
                        Dictionary<int, int> coordinates = new();
                        Dictionary<int, Dictionary<int, int>> coordinatesDictionary = new();
                        

                        if (bombFound == true)
                        {
                            if (xSquare + nextXPlusSquare < boardAConfig.BoardSizeX)
                            {
                                nextXPlusMove = xSquare + nextXPlusSquare;
                            }
                            else
                            {
                                nextXPlusMove = xSquare;
                            }

                            if (xSquare - nextXMinusSquare >= 0)
                            {
                                nextXMinusMove = xSquare - nextXMinusSquare;
                            }
                            else
                            {
                                nextXMinusMove = xSquare;
                            }

                            if (ySquare + nextYPlusSquare < boardAConfig.BoardSizeY)
                            {
                                nextYPlusMove = ySquare + nextYPlusSquare;
                            }
                            else
                            {
                                nextYPlusMove = ySquare;
                            }

                            if (ySquare - nextYMinusSquare >= 0)
                            {
                                nextYMinusMove = ySquare - nextYMinusSquare;
                            }
                            else
                            {
                                nextYMinusMove = ySquare;
                            }


                            if (boardAHidden[nextXPlusMove, ySquare].IsBomb == false &&
                                boardAHidden[nextXPlusMove, ySquare].IsShip == false)
                            {
                                boardA[nextXPlusMove, ySquare].IsBomb = true;
                                if (boardA[nextXPlusMove, ySquare].IsShip == true)
                                {
                                    boardAHidden[nextXPlusMove, ySquare].IsBomb = true;
                                    boardAHidden[nextXPlusMove, ySquare].IsShip = true;
                                    coordinates.Add(nextXPlusMove, ySquare);
                                    coordinatesDictionary.Add(hitCount, coordinates);
                                    hitSquares.Add(coordinatesDictionary);
                                    nextXPlusSquare += 1;
                                    hitCount += 1;
                                }
                                else if (boardA[nextXPlusMove, ySquare].IsShip == false)
                                {
                                    boardAHidden[nextXPlusMove, ySquare].IsBomb = true;
                                    missXSquare = nextXPlusMove;
                                    missYSquare =  ySquare;
                                    computerMoveDone = true;
                                }
                            }
                            else if (boardAHidden[nextXMinusMove, ySquare].IsBomb == false &&
                                     boardAHidden[nextXMinusMove, ySquare].IsShip == false)
                            {
                                boardA[nextXMinusMove, ySquare].IsBomb = true;
                                if (boardA[nextXMinusMove, ySquare].IsShip == true)
                                {
                                    boardAHidden[nextXMinusMove, ySquare].IsBomb = true;
                                    boardAHidden[nextXMinusMove, ySquare].IsShip = true;
                                    coordinates.Add(nextXMinusMove, ySquare);
                                    coordinatesDictionary.Add(hitCount, coordinates);
                                    hitSquares.Add(coordinatesDictionary);
                                    nextXMinusSquare += 1;
                                    hitCount += 1;
                                }
                                else if (boardA[nextXMinusMove, ySquare].IsShip == false)
                                {
                                    boardAHidden[nextXMinusMove, ySquare].IsBomb = true;
                                    missXSquare = nextXMinusMove;
                                    missYSquare =  ySquare;
                                    computerMoveDone = true;
                                }
                            }

                            else if (boardAHidden[xSquare, nextYPlusMove].IsBomb == false &&
                                     boardAHidden[xSquare, nextYPlusMove].IsShip == false)
                            {
                                boardA[xSquare, nextYPlusMove].IsBomb = true;
                                if (boardA[xSquare, nextYPlusMove].IsShip == true)
                                {
                                    boardAHidden[xSquare, nextYPlusMove].IsBomb = true;
                                    boardAHidden[xSquare, nextYPlusMove].IsShip = true;
                                    coordinates.Add(xSquare, nextYPlusMove);
                                    coordinatesDictionary.Add(hitCount, coordinates);
                                    hitSquares.Add(coordinatesDictionary);
                                    nextYPlusSquare += 1;
                                    hitCount += 1;
                                }
                                else if (boardA[xSquare, nextYPlusMove].IsShip == false)
                                {
                                    boardAHidden[xSquare, nextYPlusMove].IsBomb = true;
                                    missXSquare = xSquare;
                                    missYSquare =  nextYPlusMove;
                                    computerMoveDone = true;
                                }

                            }
                            else if (boardAHidden[xSquare, nextYMinusMove].IsBomb == false &&
                                     boardAHidden[xSquare, nextYMinusMove].IsShip == false)
                            {
                                boardA[xSquare, nextYMinusMove].IsBomb = true;
                                if (boardA[xSquare, nextYMinusMove].IsShip == true)
                                {
                                    boardAHidden[xSquare, nextYMinusMove].IsBomb = true;
                                    boardAHidden[xSquare, nextYMinusMove].IsShip = true;
                                    coordinates.Add(xSquare, nextYMinusMove);
                                    coordinatesDictionary.Add(hitCount, coordinates);
                                    hitSquares.Add(coordinatesDictionary);
                                    nextYMinusSquare += 1;
                                    hitCount += 1;
                                }
                                else
                                {
                                    boardAHidden[xSquare, nextYMinusMove].IsBomb = true;
                                    missXSquare = xSquare;
                                    missYSquare =  nextYMinusMove;
                                    computerMoveDone = true;
                                }
                            }
                        }
                        else if (bombFound == false)
                        {
                            var xHitSquare = _rnd.Next(0, boardAConfig.BoardSizeX);
                            var yHitSquare = _rnd.Next(0, boardAConfig.BoardSizeY);
                            System.Threading.Thread.Sleep(200);
                            if (boardA[xHitSquare, yHitSquare].IsBomb == false &&
                                boardA[xHitSquare, yHitSquare].IsShip)
                            {
                                boardA[xHitSquare, yHitSquare].IsBomb = true;
                                boardAHidden[xHitSquare, yHitSquare].IsBomb = true;
                                boardAHidden[xHitSquare, yHitSquare].IsShip = true;
                                coordinates.Add(xHitSquare, yHitSquare);
                                coordinatesDictionary.Add(hitCount, coordinates);
                                hitSquares.Add(coordinatesDictionary);
                                bombFound = true;
                                xSquare = xHitSquare;
                                ySquare = yHitSquare;
                                hitCount += 1;
                            }
                            else if (boardA[xHitSquare, yHitSquare].IsBomb == false &&
                                     boardA[xHitSquare, yHitSquare].IsShip == false)
                            {
                                boardA[xHitSquare, yHitSquare].IsBomb = true;
                                boardAHidden[xHitSquare, yHitSquare].IsBomb = true;
                                computerMoveDone = true;
                                missXSquare = xHitSquare;
                                missYSquare = yHitSquare;
                            }

                        }

                        
                        for (var i = 0; i < myDict.Count; i++)
                        {
                            var y = i;
                            if (myDict.ElementAt(i).Value.IsShipSunk(boardAHidden) == true && boardAHidden[coords[i][0].X, coords[i][0].Y].IsDestroyed == false)
                            {
                                Console.WriteLine(myDict.ElementAt(i).Key + " is destroyed");
                                activateFindShipsToDestroy = true;
                                nextXPlusSquare = 1;
                                nextXMinusSquare = 1;
                                nextYPlusSquare = 1;
                                nextYMinusSquare = 1;
                                nextXPlusMove = 0;
                                nextXMinusMove = 0;
                                nextYPlusMove = 0;
                                nextYMinusMove = 0;
                                bombFound = false;

                                for (var b = 0; b < coords[i].Count; b++)
                                {
                                    boardA[coords[i][b].X, coords[i][b].Y].IsDestroyed = true;
                                    boardAHidden[coords[i][b].X, coords[i][b].Y].IsDestroyed = true;
                                    if (coords[i][b].X - 1 >= 0)
                                    {
                                        boardA[coords[i][b].X - 1, coords[i][b].Y].IsBomb = true;
                                        boardAHidden[coords[i][b].X - 1, coords[i][b].Y].IsBomb = true;
                                    }

                                    if (coords[i][b].X + 1 < boardAConfig.BoardSizeX)
                                    {
                                        boardA[coords[i][b].X + 1, coords[i][b].Y].IsBomb = true;
                                        boardAHidden[coords[i][b].X + 1, coords[i][b].Y].IsBomb = true;
                                    }

                                    if (coords[i][b].Y - 1 >= 0)
                                    {
                                        boardA[coords[i][b].X, coords[i][b].Y - 1].IsBomb = true;
                                        boardAHidden[coords[i][b].X, coords[i][b].Y - 1].IsBomb = true;
                                    }

                                    if (coords[i][b].Y + 1 < boardAConfig.BoardSizeY)
                                    {
                                        boardA[coords[i][b].X, coords[i][b].Y + 1].IsBomb = true;
                                        boardAHidden[coords[i][b].X, coords[i][b].Y + 1].IsBomb = true;
                                    }

                                    if (coords[i][b].X + 1 < boardAConfig.BoardSizeX &&
                                        coords[i][b].Y + 1 < boardAConfig.BoardSizeY)
                                    {
                                        boardA[coords[i][b].X + 1, coords[i][b].Y + 1].IsBomb = true;
                                        boardAHidden[coords[i][b].X + 1, coords[i][b].Y + 1].IsBomb = true;
                                    }

                                    if (coords[i][b].X + 1 < boardAConfig.BoardSizeX &&
                                        coords[i][b].Y - 1 >= 0)
                                    {
                                        boardA[coords[i][b].X + 1, coords[i][b].Y - 1].IsBomb = true;
                                        boardAHidden[coords[i][b].X + 1, coords[i][b].Y - 1].IsBomb = true;
                                    }

                                    if (coords[i][b].X - 1 >= 0 &&
                                        coords[i][b].Y + 1 < boardAConfig.BoardSizeY)
                                    {
                                        boardA[coords[i][b].X - 1, coords[i][b].Y + 1].IsBomb = true;
                                        boardAHidden[coords[i][b].X - 1, coords[i][b].Y + 1].IsBomb = true;
                                    }

                                    if (coords[i][b].X - 1 >= 0 && coords[i][b].Y - 1 >= 0)
                                    {
                                        boardA[coords[i][b].X - 1, coords[i][b].Y - 1].IsBomb = true;
                                        boardAHidden[coords[i][b].X - 1, coords[i][b].Y - 1].IsBomb = true;
                                    }
                                }
                               
                                
                            }

                            
                            
                        }
                        var boardAShipQuantity = myDict.Count;
                        var boardADestroyedShips = 0;
                        for (var i = 0; i < myDict.Count; i++)
                        {
                            if (myDict.ElementAt(i).Value.IsShipSunk(boardA))
                            {
                                boardADestroyedShips += 1;
                                if (boardAShipQuantity == boardADestroyedShips)
                                {
                                    bWon = true;
                                    gameOver = true;
                                    computerMoveDone = true;
                                }
                            }
                        }
                    
                        var boardBShipQuantity = boardBDict.Count;
                        var boardBDestroyedShips = 0;
                        for (var i = 0; i < boardBDict.Count; i++)
                        {
                            if (boardBDict.ElementAt(i).Value.IsShipSunk(boardB))
                            {
                                boardBDestroyedShips += 1;
                                if (boardBShipQuantity == boardBDestroyedShips)
                                {
                                    aWon = true;
                                    gameOver = true;
                                    computerMoveDone = true;
                                }
                            }
                        }
                        } while (!computerMoveDone);
                    
                    hitSquaresArray = new DictionaryEntry[hitSquares.Count];
                    // hitSquares.CopyTo(hitSquaresArray, 0);

                    var aShipSquares = 0;
                    for (var i = 0; i < coordsB.Count; i++)
                    {
                        aShipSquares += coordsB[i].Count;
                    }

                    activateCode = true;

                    
                } while (!gameOver);
                
                for (var i = 0; i < boardAConfig.BoardSizeX; i++) Console.Write(alphabet[i] + " ");
                Console.WriteLine("");
                BSConsoleUI.DrawBoard(boardA);


                Console.WriteLine("---------------------------");

                for (var i = 0; i < boardBConfig.BoardSizeX; i++) Console.Write(alphabet[i] + " ");

                Console.WriteLine("");
                BSConsoleUI.DrawBoard(boardBHidden);

                if (aWon == true)
                {
                    Console.WriteLine("Player A won! Continue? Y/N");
                }
                else if (bWon == true)
                {
                    Console.WriteLine("Player B won! Continue? Y/N");
                }
                
                userChoice = Console.ReadLine()?.Trim().ToUpper();
                if (userChoice == "Y") return StartGame(brain, boardAConfigOriginal, boardBConfigOriginal);
            } while (userChoice != "N");


            return StartGame(brain, boardAConfigOriginal, boardBConfigOriginal);
        }

        public static string SavedGames()
        {
            Console.WriteLine("Enjoy your game!");
            return "";
        }

        // public static GameConfig BoatsTouch(GameConfig touchRule)
        // {
        //     if (touchRule.EShipTouchRule == EShipTouchRule.Touch)
        //     {
        //         touchRule.EShipTouchRule = EShipTouchRule.CornerTouch;
        //     }
        //     else if (touchRule.EShipTouchRule == EShipTouchRule.CornerTouch)
        //     {
        //         touchRule.EShipTouchRule = EShipTouchRule.NoTouch;
        //     }
        //     else if (touchRule.EShipTouchRule == EShipTouchRule.NoTouch)
        //     {
        //         touchRule.EShipTouchRule = EShipTouchRule.Touch;
        //     }
        //
        //
        //     return touchRule;
        // }

        
        
        
        
        

        

        

        
        
    }
}